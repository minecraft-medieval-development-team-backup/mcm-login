package com.mcm.login.cache;

import java.util.HashMap;
import java.util.UUID;

public class UUIDFetcher {

    private String nick;
    private UUID uuid;
    private static HashMap<String, UUIDFetcher> cache = new HashMap<>();

    public UUIDFetcher(String nick, UUID uuid) {
        this.nick = nick;
        this.uuid = uuid;
    }

    public static UUIDFetcher get(String nick) {
        return cache.get(nick);
    }

    public UUIDFetcher insert() {
        cache.put(nick, this);
        return this;
    }

    public UUID getUUID() {
        return uuid;
    }
}
