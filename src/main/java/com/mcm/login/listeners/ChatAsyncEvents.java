package com.mcm.login.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatAsyncEvents implements Listener {

    @EventHandler (priority = EventPriority.LOWEST)
    public void onSay(AsyncPlayerChatEvent event) {
        if (!event.getMessage().contains("/logar") && !event.getMessage().contains("/login")) {
            event.setCancelled(true);
        }
    }
}
