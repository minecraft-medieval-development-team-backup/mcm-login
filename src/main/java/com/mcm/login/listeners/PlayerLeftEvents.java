package com.mcm.login.listeners;

import com.mcm.login.database.LoginDb;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerLeftEvents implements Listener {

    @EventHandler
    public void onLeft(PlayerQuitEvent event) {
        String uuid = event.getPlayer().getUniqueId().toString();

        LoginDb.rmvCacheInfo(uuid);
        com.mcm.core.database.CoinsDb.delete(uuid);
    }

    private static void setCacheInfos(String uuid) {
        new com.mcm.core.cache.Coins(uuid, com.mcm.core.database.CoinsDb.getCoinsDb(uuid)).insert();
    }
}
