package com.mcm.login.cache;

import java.util.HashMap;

public class LoginCache {

    private static String uuid;
    private static String password;
    private static String email;
    private static String lastlogin;
    private static HashMap<String, LoginCache> cache = new HashMap<>();

    public LoginCache(String uuid, String password, String email, String lastlogin) {
        this.uuid = uuid;
        this.password = password;
        this.email = email;
        this.lastlogin = lastlogin;
    }

    public static LoginCache get(String uuid) {
        return cache.get(uuid);
    }

    public LoginCache insert() {
        cache.put(uuid, this);
        return this;
    }

    public static void set(String uuid, String password, String lastlogin) {
        if (cache.get(uuid) == null) {
            new LoginCache(uuid, password, null, lastlogin).insert();
        }
    }

    public static void remove(String uuid) {
        cache.remove(uuid);
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getLastlogin() {
        return lastlogin;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLastlogin(String lastlogin) {
        this.lastlogin = lastlogin;
    }
}

