package com.mcm.login.utils;

public class CentralizerText {

    public static String centralize(String str, int i, String f) {
        int c = 0;
        while (str.length() < i) {
            if (c == 0) {
                str = str + f;
                c = 1;
            } else if (c == 1) {
                str = f + str;
                c = 0;
            } else {
                c = 0;
            }
        }
        return str;
    }
}
