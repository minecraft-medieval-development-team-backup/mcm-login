package com.mcm.login.commands;

import com.mcm.login.cache.LoginSystemStatus;
import com.mcm.login.database.LoginStatusDb;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandUpdate implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor"};
        if (command.getName().equalsIgnoreCase("svupdate") && Arrays.asList(perm).contains(tag)) {
            // * Atualizations *
            LoginSystemStatus.get().setStatus(LoginStatusDb.getStatus());
            // * - *

            if (player != null) {
                player.sendMessage(ChatColor.GREEN + " * Servidor atualizado com sucesso!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else System.out.println(ChatColor.GREEN + " * Servidor atualizado com sucesso!");
        }
        return false;
    }
}
