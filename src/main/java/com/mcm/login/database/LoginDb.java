package com.mcm.login.database;

import com.google.common.hash.Hashing;
import com.mcm.login.MySql;
import com.mcm.login.cache.LoginCache;

import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginDb {

    public static String hashPassword(String password) {
        return Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString();
    }

    public static String getPassword(String uuid) {
        if (LoginCache.get(uuid) != null) {
            return LoginCache.get(uuid).getPassword();
        } else try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Login_data WHERE uuid = ?");
            statement.setString(1, uuid);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String password = rs.getString("password");
                LoginCache.set(uuid, password, "SETAR-LAST-LOGIN");
                return password;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void setNewAccount(String uuid, String password, String lastlogin) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Login_data(uuid, password, email, lastlogin) VALUES (?, ?, ?, ?)");
            statement.setString(1, uuid);
            statement.setString(2, hashPassword(password));
            statement.setString(3, null);
            statement.setString(4, lastlogin);
            statement.executeUpdate();

            LoginCache.set(uuid, password, lastlogin);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        com.mcm.core.database.CoinsDb.set(uuid);
    }

    public static void rmvCacheInfo(String uuid) {
        LoginCache.remove(uuid);
    }

    public static String getEmail(String uuid) {
        return LoginCache.get(uuid).getEmail();
    }

    public static void setEmail(String uuid, String email) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Login_data SET email = ? WHERE uuid = ?");
            statement.setString(2, uuid);
            statement.setString(1, email);
            statement.executeUpdate();

            LoginCache.get(uuid).setEmail(email);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getLastlogin(String uuid) {
        return LoginCache.get(uuid).getLastlogin();
    }

    public static void setLastlogin(String uuid, String lastlogin) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Login_data SET lastlogin = ? WHERE uuid = ?");
            statement.setString(2, uuid);
            statement.setString(1, lastlogin);
            statement.executeUpdate();

            LoginCache.get(uuid).setLastlogin(lastlogin);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
