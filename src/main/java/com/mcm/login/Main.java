package com.mcm.login;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Main instance;
    public static Plugin plugin;

    @Override
    public void onEnable() {
        Main.instance = this;
        Main.plugin = this;
        MySql.connect();
        RegisterCommands.register();
        RegisterEvents.register();
        RegisterMain.register();
    }

    @Override
    public void onDisable() {
        MySql.disconnect();
    }
}
