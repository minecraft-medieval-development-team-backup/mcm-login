package com.mcm.login.database;

import com.mcm.core.Redis;
import com.mcm.login.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OriginalLogged {

    public static String getLogged(String uuid) {
        return Redis.localhost.get(uuid + "originallogged");
    }

    public static void removeLogged(String uuid) {
        Redis.localhost.del(uuid + "originallogged");
    }
}
