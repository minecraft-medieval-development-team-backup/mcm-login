package com.mcm.login.cache;

import java.util.HashMap;

public class LoginSystemStatus {

    private String server;
    private boolean status;
    private static HashMap<String, LoginSystemStatus> cache = new HashMap<String, LoginSystemStatus>();

    public LoginSystemStatus(String server, boolean status) {
        this.server = server;
        this.status = status;
    }

    public static LoginSystemStatus get() {
        return cache.get("server");
    }

    public LoginSystemStatus insert() {
        cache.put(server, this);
        return this;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
