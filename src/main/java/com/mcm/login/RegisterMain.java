package com.mcm.login;

import com.mcm.login.cache.LoginSystemStatus;
import com.mcm.login.database.LoginStatusDb;

public class RegisterMain {

    public static void register() {
        new LoginSystemStatus("server", LoginStatusDb.getStatus()).insert();
    }
}
