package com.mcm.login.commands;

import com.mcm.core.Main;
import com.mcm.login.cache.Attempts;
import com.mcm.login.cache.LoginSystemStatus;
import com.mcm.login.database.LoginDb;
import com.mcm.login.utils.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandLogin implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        if (command.getName().equalsIgnoreCase("logar") || command.getName().equalsIgnoreCase("login")) {
            if (LoginDb.getPassword(uuid) != null) {
                if (UUIDFetcher.getUUID(player.getName()) == null) {
                    login(args, player, uuid);
                } else {
                    if (LoginSystemStatus.get().getStatus() == true) {
                        player.sendMessage(Main.getTradution("pPXYSY2cTwU4%?8", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    } else {
                        login(args, player, uuid);
                    }
                }
            } else {
                if (UUIDFetcher.getUUID(player.getName()) == null || LoginSystemStatus.get().getStatus() == false) {
                    player.sendMessage(Main.getTradution("J4XyT?GB5e6ZT%v", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }

    private static void login(String[] args, Player player, String uuid) {
        if (args.length == 1) {
            if (LoginDb.getPassword(uuid).equalsIgnoreCase(LoginDb.hashPassword(args[0]))) {
                player.sendMessage(Main.getTradution("Yc!xB4euhKHEj!3", uuid));
                player.playSound(player.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 100, 0.723f);
                showAll(player);
            } else {
                player.sendMessage(Main.getTradution("ks634V?ke%v*uC%", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);

                if (Attempts.get(uuid) == null) new Attempts(uuid).insert(); else Attempts.get(uuid).add();
                if (Attempts.get(uuid) != null && Attempts.get(uuid).getTimes() == 4) {
                    //TODO * Kick the player for 5 minutes
                }
            }
        } else if (args.length > 1) {
            player.sendMessage(Main.getTradution("D*yKezyhV32fdnZ", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }

    private static void showAll(Player player) {
        for (Player target : Bukkit.getOnlinePlayers()) {
            target.showPlayer(Main.plugin, player);
            player.showPlayer(Main.plugin, target);
        }
    }
}
