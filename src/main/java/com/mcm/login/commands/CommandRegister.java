package com.mcm.login.commands;

import com.mcm.core.Main;
import com.mcm.login.cache.LoginSystemStatus;
import com.mcm.login.database.LoginDb;
import com.mcm.login.utils.AlternateColor;
import com.mcm.login.utils.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Date;

public class CommandRegister implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        if (command.getName().equalsIgnoreCase("registrar") || command.getName().equalsIgnoreCase("register")) {
            if (LoginDb.getPassword(uuid) == null) {
                if (UUIDFetcher.getUUID(player.getName()) == null) {
                    register(args, player, uuid);
                } else {
                    if (LoginSystemStatus.get().getStatus() == true) {
                        player.sendMessage(Main.getTradution("Udrd24hXWC$kUXP", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    } else {
                        register(args, player, uuid);
                    }
                }
            } else {
                player.sendMessage(Main.getTradution("@shHw%CJHYvC6#f", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        }
        return false;
    }

    private static void register(String[] args, Player player, String uuid) {
        if (args.length == 1) {
            Date date = new Date();
            LoginDb.setNewAccount(uuid, args[0], date.getDay() + "/" + date.getMonth() + "/" + date.getYear() + "-" + date.getHours() + ":" + date.getMinutes());

            showAll(player);
            player.sendMessage(Main.getTradution("Yc!xB4euhKHEj!3", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

            Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                if (LoginDb.getEmail(uuid) == null) {
                    player.sendMessage(AlternateColor.alternate(Main.getTradution("&Yy4cu*jp2gUYAS", uuid)));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }, 60L);
        } else {
            player.sendMessage(Main.getTradution("u3BW5H&pmAQ7S#y", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }

    private static void showAll(Player player) {
        for (Player target : Bukkit.getOnlinePlayers()) {
            target.showPlayer(Main.plugin, player);
            player.showPlayer(Main.plugin, target);
        }
    }
}
