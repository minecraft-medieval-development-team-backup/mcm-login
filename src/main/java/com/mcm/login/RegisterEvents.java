package com.mcm.login;

import com.mcm.login.listeners.PlayerJoinEvents;
import com.mcm.login.listeners.PlayerLeftEvents;
import org.bukkit.Bukkit;

public class RegisterEvents {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new PlayerJoinEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerLeftEvents(), Main.plugin);
    }
}
