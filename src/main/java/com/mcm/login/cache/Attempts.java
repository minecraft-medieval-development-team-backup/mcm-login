package com.mcm.login.cache;

import java.util.HashMap;

public class Attempts {

    private String uuid;
    private int times;
    private static HashMap<String, Attempts> cache = new HashMap<>();

    public Attempts (String uuid) {
        this.uuid = uuid;
        this.times = 1;
    }

    public Attempts insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static Attempts get(String uuid) {
        return cache.get(uuid);
    }

    public int getTimes() {
        return times;
    }

    public void add() {
        this.times++;
    }
}
