package com.mcm.login;

import com.mcm.login.commands.CommandLogin;
import com.mcm.login.commands.CommandRegister;
import com.mcm.login.commands.CommandUpdate;

public class RegisterCommands {

    public static void register() {
        Main.instance.getCommand("svupdate").setExecutor(new CommandUpdate());
        Main.instance.getCommand("registrar").setExecutor(new CommandRegister());
        Main.instance.getCommand("register").setExecutor(new CommandRegister());
        Main.instance.getCommand("logar").setExecutor(new CommandLogin());
        Main.instance.getCommand("login").setExecutor(new CommandLogin());
    }
}
