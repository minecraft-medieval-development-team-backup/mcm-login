package com.mcm.login.database;

import com.mcm.login.MySql;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginStatusDb {

    public static boolean getStatus() {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM LoginStatus WHERE server = ?");
            statement.setString(1, "server");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("status");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
