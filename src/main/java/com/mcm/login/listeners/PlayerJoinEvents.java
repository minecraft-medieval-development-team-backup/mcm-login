package com.mcm.login.listeners;

import com.mcm.core.cache.isSwitch;
import com.mcm.core.Main;
import com.mcm.login.cache.LoginSystemStatus;
import com.mcm.login.database.LoginDb;
import com.mcm.login.database.OriginalLogged;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinEvents implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        setCacheInfos(uuid);

        //TODO * Teleport to spawn here
        event.setJoinMessage(null);

        Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
            if (isSwitch.get(uuid) == null) {
                for (int i = 0; i < 100; i++) {
                    player.sendMessage(" ");
                }

                if (LoginSystemStatus.get().getStatus() == false) {
                    if (LoginDb.getPassword(uuid) == null) {
                        hideAll(player);
                        player.sendMessage(Main.getTradution("H3?9fBp4Ae5d8%j", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        hideAll(player);
                        player.sendMessage(Main.getTradution("XWeSX*@Crh6s?Wc", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    if (OriginalLogged.getLogged(player.getName()) != null) {
                        player.sendMessage(Main.getTradution("Yc!xB4euhKHEj!3", uuid));
                        player.playSound(player.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 100, 0.723f);
                        OriginalLogged.removeLogged(uuid);
                    } else if (LoginDb.getPassword(uuid) == null) {
                        hideAll(player);
                        player.sendMessage(Main.getTradution("H3?9fBp4Ae5d8%j", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        hideAll(player);
                        player.sendMessage(Main.getTradution("XWeSX*@Crh6s?Wc", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }
            } else {
                for (int i = 0; i < 100; i++) {
                    player.sendMessage(" ");
                }
                player.sendMessage(ChatColor.YELLOW + " \n " + Main.getTradution("rTT@xR9GbAC@g@a", uuid) + "\n ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                isSwitch.get(uuid).delete();
            }
        }, 45);
    }

    private static void hideAll(Player player) {
        for (Player target : Bukkit.getOnlinePlayers()) {
            target.hidePlayer(Main.plugin, player);
            player.hidePlayer(Main.plugin, target);
        }
    }

    private static void setCacheInfos(String uuid) {
        new com.mcm.core.cache.Coins(uuid, com.mcm.core.database.CoinsDb.getCoinsDb(uuid)).insert();
    }
}
